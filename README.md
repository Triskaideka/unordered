# Truly Unordered Lists

I was struck by the thought: why do we call them un*ordered* lists?
They *are* in an order.  They can't *not* be in an order.
What we mean is that they're un*numbered* lists,
or that the order they're in isn't significant.
But what would it mean to have truly un*ordered* lists?
How could we emphasize that the order doesn't matter?
Well, we could shuffle them each time the page is loaded.

[See this in action.](https://triskaideka.net/unordered/)

## How to use

Include truly_unordered_lists.js in your HTML page.
The rest will happen automatically.

    <script type="text/javascript" src="truly_unordered_lists.js"></script>

If you're a troublemaker, uncomment the "setInterval" line at the bottom of
the JS file to re-shuffle the lists every ten seconds.

## Why to use

???
