/*
truly_unordered_lists.js
Programmer: Triskaideka ( https://triskaideka.net/ )

Shuffle the order of the LI items within each UL, on page load.
 */

// Do the work of disordering each list on the page.
function tul_unorder_lists() {

  let lists = document.getElementsByTagName("ul");

  for (list of lists) {

    let i = 0
      , items = list.getElementsByTagName("li")
      , replacement = []
      ;

    // Repeatedly put a random list item into the replacement list,
    // then delete it.
    while (items.length > 0) {
      i = Math.floor(Math.random() * items.length);
      replacement.push(items[i]);
      items[i].remove();
    }

    // Replace the original list with the replacement.
    for (li of replacement) {
      list.append(li);
    }

  }

}



// Register event listeners.
function tul_ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}



// Run the tul_unorder_lists function when the page loads.
tul_ready(tul_unorder_lists);



// If you're feeling evil, uncomment this line to re-shuffle the lists
// every ten seconds.
//window.setInterval(tul_unorder_lists, 10000);
